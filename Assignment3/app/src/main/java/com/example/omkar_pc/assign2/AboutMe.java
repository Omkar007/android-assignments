package com.example.omkar_pc.assign2;

//import android.app.Fragment;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.ViewGroup;
import android.os.Bundle;
import android.view.LayoutInflater;
/**
 * Created by Omkar Patil on 2/9/17.
 */

public class AboutMe extends Fragment {
    public AboutMe(){}

    public View onCreateView ( LayoutInflater inflater , ViewGroup
            container , Bundle savedInstanceState ) {
// Inflate the layout for this fragment
        return inflater . inflate ( R.layout.aboutme , container , false );
    }
}
