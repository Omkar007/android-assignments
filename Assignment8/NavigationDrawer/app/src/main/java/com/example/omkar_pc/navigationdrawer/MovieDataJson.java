package com.example.omkar_pc.navigationdrawer;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by omkar on 3/23/2017.
 */

public class MovieDataJson {
    private String addURL="http://omkar.com/index.php/add";
    private String deleteURL="http://omkar.com/index.php/delete";
    List<Map<String, ?>> moviesList;
    MyUtility utility=new MyUtility();

    public List<Map<String, ?>> getMoviesList() {
        return moviesList;
    }

    public int getSize() {
        return moviesList.size();
    }

    public HashMap getItem(int i) {
        if (i >= 0 && i < moviesList.size()) {
            return (HashMap) moviesList.get(i);
        } else return null;
    }

    public int getPosition(String movieName) {
        int position = -1;
        for (Map<String, ?> movieData : moviesList) {
            position++;
            String s = (String) movieData.get("name");
            if (s.toLowerCase().contains(movieName.toLowerCase()))
                return position;
        }
        return -1;
    }

    public MovieDataJson() {
        String id;
        String description;
        String length;
        String year;
        double rating;
        String director;
        String stars;
        String url;
        moviesList = new ArrayList<Map<String, ?>>();

    }

    public void downLoadMoviedataJson(String url) throws JSONException {
       String jsonStr=utility.downloadJSONusingHTTPGetRequest(url);
        Log.d("url ",url);
        if(jsonStr!=null){
            Log.d("string from db",jsonStr);
            JSONArray objArray=new JSONArray(jsonStr);
            for(int i=0;i<objArray.length();i++){
                moviesList.add(createMovie(objArray.getJSONObject(i).getString("id"),objArray.getJSONObject(i).getString("name"),0,objArray.getJSONObject(i).getString("description"),objArray.getJSONObject(i).getString("year"),objArray.getJSONObject(i).getString("length"),objArray.getJSONObject(i).getDouble("rating"),objArray.getJSONObject(i).getString("director"),objArray.getJSONObject(i).getString("stars"),objArray.getJSONObject(i).getString("url")));
            }
        }
    }
    public void ADDtoDB(int position,HashMap hs)
    {
        final JSONObject json;
        if(hs!=null)
        {
            String newname= (String) hs.get("id");
            newname=newname+"_new";
            hs.put("id",newname);

            json=new JSONObject(hs);
            Log.d("JSON TO ADD DB:",""+json);
        }
        else json=null;
        Runnable runnable=new Runnable() {
            @Override
            public void run() {
                MyUtility.sendHttPostRequest(addURL,json);
            }
        };
        new Thread(runnable).start();
    }

    public void deletefromDB(int position,HashMap hs)
    {
        final JSONObject json;
        json=new JSONObject(hs);
        Log.d("json movie to delete===",""+json);
        Runnable runnable=new Runnable() {
            @Override
            public void run() {
                MyUtility.sendHttPostRequest(deleteURL,json);
            }
        };
        new Thread(runnable).start();
    }
    private HashMap createMovie(String id,String name, int image, String description, String year, String length, double rating, String director, String stars, String url) {
        HashMap movie = new HashMap();
        movie.put("id",id);
        movie.put("image", image);
        movie.put("name", name);
        movie.put("description", description);
        movie.put("year", year);
        movie.put("length", length);
        movie.put("rating", rating);
        movie.put("director", director);
        movie.put("stars", stars);
        movie.put("url", url);
        movie.put("selection", false);
        return movie;
    }
}
