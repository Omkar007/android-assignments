package com.example.omkar_pc.assign2;

/**
 * Created by Omkar-PC on 16-02-2017.
 */
import android.support.annotation.BoolRes;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPropertyAnimatorListener;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.ImageView;
import android.view.LayoutInflater;
import android.widget.CheckBox;
import java.util.HashMap;
import android.support.v4.app.Fragment;

import jp.wasabeef.recyclerview.animators.holder.AnimateViewHolder;

public class MyRVAdapter extends RecyclerView.Adapter<MyRVAdapter.ViewHolder> {

    private static MovieData mItems;
    private int lastPosition = -1;
    static OnItemClickListener mItemClickListener;

    public MyRVAdapter(MovieData movieList) {
        mItems = movieList;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements AnimateViewHolder {
        public ImageView movieImage;
        public TextView movieName;
        public TextView movieDesc;
        public CheckBox movieCheck;
        RatingBar ratingBar;

        public ViewHolder(View view) {
            super(view);
            movieImage = (ImageView) view.findViewById(R.id.movieImage);
            movieName = (TextView) view.findViewById(R.id.movieName);
            movieDesc = (TextView) view.findViewById(R.id.movieDesc);
            movieCheck = (CheckBox) view.findViewById(R.id.movieCheck);
            ratingBar = (RatingBar) view.findViewById(R.id.rat_val);

            view.setOnClickListener ( new View . OnClickListener () {
                @Override
                public void onClick ( View v ) {
                    if( mItemClickListener != null ) {
                        if( getAdapterPosition () !=
                                RecyclerView . NO_POSITION ) {
                            mItemClickListener.onItemClick (v,getAdapterPosition());
                        }
                    }
                }
            }) ;

            view . setOnLongClickListener ( new View . OnLongClickListener () {

                                                        @Override
                                                        public boolean onLongClick ( View v) {
                                                            if( mItemClickListener != null ) {
                                                                if( getAdapterPosition () !=
                                                                        RecyclerView . NO_POSITION ) {
                                                                    mItemClickListener.onItemLongClick (v ,
                                                                            getAdapterPosition () );
                                                                }
                                                            }
                                                            return true ;
                                                        }
                                                    }) ;
            movieCheck.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(movieCheck.isChecked()) {
                        HashMap map = (HashMap) mItems.getMoviesList().get(getAdapterPosition());
                        map.put("selection", true);
                        mItems.getMoviesList().set(getAdapterPosition(), map);
                    }
                    else
                    {
                        HashMap map = (HashMap) mItems.getMoviesList().get(getAdapterPosition());
                        map.put("selection", false);
                        mItems.getMoviesList().set(getAdapterPosition(), map);
                    }
                }
            });
        }

        @Override
        public void preAnimateAddImpl(RecyclerView.ViewHolder holder) {
            ViewCompat.setTranslationY(itemView, -itemView.getHeight() * 0.3f);
            ViewCompat.setAlpha(itemView, 0);
        }

        @Override
        public void preAnimateRemoveImpl(RecyclerView.ViewHolder holder) {

        }

        @Override
        public void animateAddImpl(RecyclerView.ViewHolder holder, ViewPropertyAnimatorListener listener) {
            ViewCompat.animate(itemView)
                    .translationY(0)
                    .alpha(1)
                    .setDuration(300)
                    .setListener(listener)
                    .start();
        }

        @Override
        public void animateRemoveImpl(RecyclerView.ViewHolder holder, ViewPropertyAnimatorListener listener) {
            ViewCompat.animate(itemView)
                    .translationY(-itemView.getHeight() * 0.3f)
                    .alpha(0)
                    .setDuration(300)
                    .setListener(listener)
                    .start();
        }
    }
            @Override
            public ViewHolder onCreateViewHolder(ViewGroup parent,int viewType){
                View v;
                v = LayoutInflater.from(parent.getContext()).inflate(
                        R.layout.rv_item, parent, false);
                return new ViewHolder(v);
            }

    public interface OnItemClickListener {
        public void onItemClick ( View view , int position );
        public void onItemLongClick ( View view , int position );
    }

    public void setOnItemClickListener ( final OnItemClickListener
                                                 mItemClickListener ) {
        this . mItemClickListener = mItemClickListener ;
    }

    @Override
    public void onBindViewHolder (ViewHolder holder , final int position ) {
        final HashMap < String ,? > movie = mItems.getItem( position );
        holder.movieImage.setImageResource(( Integer )movie.get ("image")) ;
        holder.movieName.setText (( String ) movie . get ("name"));
        holder.movieDesc. setText (( String ) movie . get ("description") );
        float rating = Float.parseFloat(Double.toString ((Double)movie.get("rating")));
        holder.ratingBar.setRating(rating/2);

        if(movie.get("selection")==Boolean.TRUE){
            holder.movieCheck.setChecked ( true );
        }
        else{
            holder.movieCheck.setChecked ( false );
        }
    }

    @Override
    public int getItemCount () {
        return mItems . getSize () ;
    }
    @Override
    public int getItemViewType ( int position ){
        return position % 3;
    }

}