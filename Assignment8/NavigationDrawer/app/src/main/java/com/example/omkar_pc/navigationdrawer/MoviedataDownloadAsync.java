package com.example.omkar_pc.navigationdrawer;

import android.os.AsyncTask;

import org.json.JSONException;

import java.lang.ref.WeakReference;

/**
 * Created by omkar on 3/23/2017.
 */

public class MoviedataDownloadAsync extends AsyncTask<String,Void,MovieDataJson> {
    private final WeakReference<RecyclerAdapter> adapterRef;
    private MovieData movieData;
    public MoviedataDownloadAsync(RecyclerAdapter adapter,MovieData movieData) {
        adapterRef=new WeakReference<RecyclerAdapter>(adapter);
        this.movieData=movieData;
    }

    @Override
    protected MovieDataJson doInBackground(String... urls) {
       MovieDataJson thMoviedata=new MovieDataJson();

        for(String url:urls){
            try {
                thMoviedata.downLoadMoviedataJson(url);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return thMoviedata;
    }

    @Override
    protected void onPostExecute(MovieDataJson thMoviedata) {
        movieData.moviesList.clear();
        for(int i=0;i<thMoviedata.getSize();i++){
            movieData.moviesList.add(thMoviedata.moviesList.get(i));
        }
        if(adapterRef!=null){
            final RecyclerAdapter adapter=adapterRef.get();
            if(adapter!=null){
                adapter.notifyDataSetChanged();
            }
        }
    }
}
