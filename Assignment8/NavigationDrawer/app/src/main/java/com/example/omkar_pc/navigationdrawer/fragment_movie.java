package com.example.omkar_pc.navigationdrawer;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.util.LruCache;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.Map;

import static android.content.Context.CLIPBOARD_SERVICE;

/**
 * A simple {@link Fragment} subclass.
 */
public class fragment_movie extends Fragment {

    private int page;
    Integer startIndex = 0;
    static MovieData data;
    static LruCache<String,Bitmap> mCacheImg;
    static RecyclerAdapter adapter;
    ImageView imageView;
    TextView movie_name, movie_desc, movie_year,movie_len, movie_stars;
    RatingBar ratingBar;
    private ActionMode acMode;
    ClipboardManager clipMgr;
    public static fragment_movie newInstance (int page, MovieData md,RecyclerAdapter adapter1,LruCache<String,Bitmap> mImgMemory) {
        data = md;
        adapter=adapter1;
        mCacheImg=mImgMemory;
        fragment_movie fragmentFirst = new fragment_movie();
        Bundle args = new Bundle();
        args.putInt("PageNo", page);
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        clipMgr = (ClipboardManager) this.getContext().getSystemService(CLIPBOARD_SERVICE);
        View rootView = inflater.inflate(R.layout.movie_fragment, container, false);
        startIndex = data.getSize();

        movie_name = (TextView) rootView.findViewById(R.id.movie_name_val);
        movie_desc = (TextView) rootView.findViewById(R.id.movie_desc_val);
        movie_year = (TextView) rootView.findViewById(R.id.movie_year_val);
        movie_len = (TextView) rootView.findViewById(R.id.movie_len_val);
        movie_stars = (TextView) rootView.findViewById(R.id.movie_star_val);
        ratingBar = (RatingBar) rootView.findViewById(R.id.ratingBar);
        movie_desc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(acMode == null)
                    acMode = view.startActionMode(new CopyPasteCallback());
            }

        });
        ratingBar.setOnTouchListener(new OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });


        page = getArguments().getInt("PageNo", 0);
        imageView = (ImageView) rootView.findViewById(R.id.imageView);
        ChangeMovieData(page);

        return rootView;
    }

    void ChangeMovieData(Integer pos)
    {
        Map<String, ?> myMap = data.getItem(pos);

       // imageView.setImageResource((Integer)myMap.get("image"));
        GetMovieListAysncTask task=new GetMovieListAysncTask(imageView,mCacheImg);
        task.execute((String)myMap.get("url"));
        movie_name.setText((String) myMap.get("name"));
        movie_desc.setText((String) myMap.get("description"));
        movie_year.setText((String) myMap.get("year"));
        movie_len.setText((String) myMap.get("length"));
        movie_stars.setText((String) myMap.get("stars"));
        ratingBar.setRating((((Double) myMap.get("rating")).floatValue())/2);
    }
    class CopyPasteCallback implements ActionMode.Callback {

        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate(R.menu.copy_cab, menu);
            return true;
        }

        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch(item.getItemId()) {

                case R.id.copy_item:
                    infoCopy();
                    mode.finish();
                    return true;
                case R.id.paste_item:
                    infoPaste();
                    mode.finish();
                    return true;
            }
            return false;
        }

        public void onDestroyActionMode(ActionMode mode) {
            acMode = null;
        }
    }

    public void infoCopy(){
        ClipData clipData = ClipData.newPlainText(movie_desc.toString(), movie_desc.getText().toString());
        clipMgr.setPrimaryClip(clipData);
    }

    public void infoPaste(){
        ClipData dataClip = clipMgr.getPrimaryClip();
        String  infoToPaste=dataClip.getItemAt(0).getText().toString();
        movie_desc.setText(infoToPaste);

    }
}
