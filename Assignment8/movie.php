<?php

//db connect function
function getDB() {
	 $dbhost = "localhost";
	 $dbuser = "root";
	 $dbpass = "";
	 $dbname = "moviedb";
	 
	 $conn = new mysqli($dbhost,$dbuser,$dbpass,$dbname);
	 if($conn->connect_error) {
		 die("connection failed: ". $conn->connect_error . "\n");
	 }
	 
	 return $conn;
}

//Function for Movie Details List 
 function getMovieList(){
   $conn =getDB();
   $sql = "SELECT * FROM movies ORDER BY name ASC";
   $result=$conn->query($sql);
   $jason=array();
   while($row = $result->fetch_assoc()){
   $row_array['name']= $row['name'];
   $row_array['id']= $row['id'];
   $row_array['description']= $row['description'];
   $row_array['rating']= $row['rating'];
   $row_array['url']= $row['url'];
   $row_array['director']= $row['director'];
   $row_array['length']= $row['length'];
   $row_array['year']= $row['year'];
   $row_array['stars']= $row['stars'];
      array_push($jason,$row_array);
   }
   echo json_encode($jason);
   $conn->close();
   
 }
 
 
 //Function to get Movie Details
 function getMovieDetails($movieId){
 $conn =getDB();
 $sql = "SELECT * FROM movies where id='".$movieId."'";
 $result=$conn->query($sql);
 $jason=array();
 while($row = $result->fetch_assoc()){
   $row_array['name']= $row['name'];
   $row_array['id']= $row['id'];
   $row_array['description']= $row['description'];
   $row_array['rating']= $row['rating'];
   $row_array['url']= $row['url'];
   $row_array['director']= $row['director'];
   $row_array['length']= $row['length'];
   $row_array['year']= $row['year'];
   $row_array['stars']= $row['stars'];
 array_push($jason,$row_array);
 }
 echo json_encode($jason);
 $conn->close();
 }
 
 
 //Function to get Movie Rating
 function getMoviesWithRating($movieRating){
   $conn =getDB();
   $stmt=$conn->prepare("SELECT * FROM `movies` where rating>? ORDER BY rating DESC");
   $stmt->bind_param("s",$movieRating);
   $stmt->execute();
   $result=$stmt->get_result();
   $jason1=array();
   while($row = $result->fetch_assoc()){
   $row_array['name']= $row['name'];
   $row_array['id']= $row['id'];
   $row_array['description']= $row['description'];
   $row_array['rating']= $row['rating'];
   $row_array['url']= $row['url'];
   $row_array['director']= $row['director'];
   $row_array['length']= $row['length'];
   $row_array['year']= $row['year'];
   $row_array['stars']= $row['stars'];
      array_push($jason1,$row_array);
   }
   echo json_encode($jason1);
   $conn->close();
 }
 

?>