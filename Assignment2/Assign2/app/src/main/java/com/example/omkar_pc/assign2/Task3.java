
package com.example.omkar_pc.assign2;

import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Toast;
import android.widget.TextView;

import java.util.Map;

public class Task3 extends AppCompatActivity {

    Integer start = 0;
    Integer MaxValue = 0;
    MovieData data = null;
    TextView movie_name, movie_desc, movie_year, movie_len, movie_rat, movie_dir, movie_stars, movie_url;
    ImageView ImgView;
    SeekBar seekBarMover;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task3);

        seekBarMover = (SeekBar) findViewById(R.id.skBar);
        ImgView = (ImageView) findViewById(R.id.imageView);
        ImgView.getLayoutParams().height = 500;

        data = new MovieData();
        MaxValue = data.getSize();
        movie_dir = (TextView) findViewById(R.id.dir_val);
        movie_url = (TextView) findViewById(R.id.url_val);
        movie_name = (TextView) findViewById(R.id.name_val);
        movie_desc = (TextView) findViewById(R.id.desc_val);
        movie_len = (TextView) findViewById(R.id.len_val);
        movie_rat = (TextView) findViewById(R.id.rat_val);
        movie_stars = (TextView) findViewById(R.id.star_val);
        movie_year = (TextView) findViewById(R.id.year_val);
        ImageSlider(0);
        ImgView.requestLayout();

        seekBarMover.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progressChanged = 0;

            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progressChanged = progress;
                ImgView.getLayoutParams().height = (1000 * progressChanged / 100);
                ImgView.requestLayout();
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
                ImgView.getLayoutParams().height = (1000 * progressChanged / 100);
                ImgView.requestLayout();
            }
        });

        ImgView.setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {

                seekBarMover.setProgress(50);
                ImgView.getLayoutParams().height = 500;
                ImgView.requestLayout();
                return true;
            }
        });

        ImgView.setOnTouchListener(new ImageSwipeSlider(Task3.this)
        {
            public void onSwipeLeft() {
                if(start != MaxValue) {
                    ++start;
                    ImageSlider(start);
                }
            }

            public void onSwipeRight() {
                if(start != 0) {
                    --start;
                    ImageSlider(start);
                }
            }

            public void thisisClick()
            {
                Snackbar.make(findViewById(R.id.activity_task3), movie_name.getText(), Snackbar.LENGTH_SHORT).show();
                Toast.makeText(Task3.this, movie_name.getText(), Toast.LENGTH_SHORT).show();
            }
            public void thisisLongPress()
            {
                seekBarMover.setProgress(50);
                ImgView.getLayoutParams().height = 500;
                ImgView.requestLayout();
            }
        });
    }


    void ImageSlider(Integer Index)
    {
        Map<String, ?> myMap = data.getItem(Index);

        ImgView.setImageResource((Integer)myMap.get("image"));
        movie_len.setText((String) myMap.get("length"));
        movie_rat.setText(Double.toString ((Double) myMap.get("rating")));
        movie_dir.setText((String) myMap.get("director"));
        movie_name.setText((String) myMap.get("name"));
        movie_desc.setText((String) myMap.get("description"));
        movie_year.setText((String) myMap.get("year"));
        movie_stars.setText((String) myMap.get("stars"));
        movie_url.setText((String) myMap.get("url"));
    }
}
