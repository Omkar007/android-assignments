package com.example.omkar_pc.assign2;

//import android.app.Fragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by omkar patil on 2/10/17.
 */

public class MasterDetailFragment extends Fragment implements View.OnClickListener{
    int max_val=0;
    public MasterDetailFragment(){}
    private TextView movieIndex;
    Button buttonInc,buttonDec;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parentViewGroup,
                             Bundle savedInstanceState) {

        View rootView =  inflater . inflate ( R.layout.master_fragment , parentViewGroup , false );

        buttonInc=(Button) rootView.findViewById(R.id.button2);
        buttonDec=(Button) rootView.findViewById(R.id.button4);
        buttonInc.setOnClickListener(this);
        buttonDec.setOnClickListener(this);
        movieIndex=(TextView) rootView.findViewById(R.id.indexTextView);
        movieIndex.setText(String.valueOf(max_val));
        return rootView;

    }
    public void setText ( String text ) {
        movieIndex . setText ( text );
    }
    @Override
    public void onClick(View v) {
        Fragment fragment = null;

        switch (v.getId()) {
            case R.id.button2:
               if(max_val<30){
                   max_val++;
                   Log.i("value",String.valueOf(max_val));
                   Log.i("max",String.valueOf(movieIndex));
                   movieIndex.setText(String.valueOf(max_val));
                   android.support.v4.app.FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                   ft . replace (R . id .movieFrag , new moviefragment() );
                   ft . commit () ;
               }
                break;
            case R.id.button4:
                if(max_val>0 && max_val!=0){
                    max_val--;
                    Log.d("min",String.valueOf(max_val));
                    movieIndex.setText(String.valueOf(max_val));
                    System.out.println(movieIndex);
                    android.support.v4.app.FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                    ft . replace (R . id .movieFrag , new moviefragment() );
                    ft . commit () ;
              }
                break;
            default:
                break;
        }
    }
}
