package com.example.omkar_pc.navigationdrawer;

import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private DrawerLayout mDrawerLayout;
    private NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.navigation_view);
        ActionBarDrawerToggle actionBarDrawerToggle=new ActionBarDrawerToggle(this,mDrawerLayout,toolbar ,R.string.open, R.string.close){
            @Override
            public void onDrawerClosed(View drawerView){
                super.onDrawerClosed(drawerView);
            }
            @Override
            public void onDrawerOpened(View drawerView){
                super.onDrawerOpened(drawerView);
            }
        };

        mDrawerLayout.setDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                menuItem.setChecked(true);
                mDrawerLayout.closeDrawers();
                Intent intent;
                switch (menuItem.getItemId()) {

                    case R.id.nav_first_activity:
                        Fragment fragment = null;
                        fragment = new AboutMe();
                        if ( findViewById (R.id.frame_container ) != null && fragment !=
                                null ) {
                            getSupportFragmentManager().beginTransaction ().add (R.id.frame_container ,
                                    fragment ). commit () ;
                        }
                        break;

                    case R.id.nav_second_activity:
                        Toast.makeText(MainActivity.this, "Navigation Item 02 Clicked", Toast.LENGTH_SHORT).show();
                        intent=new Intent(MainActivity.this, Task2.class);
                        startActivity(intent);
                        break;

                    case R.id.nav_third_activity:
                        Toast.makeText(MainActivity.this, "Navigation Item 03 Clicked", Toast.LENGTH_SHORT).show();
                        intent=new Intent(MainActivity.this, Task3.class);
                        startActivity(intent);
                        break;
                    case R.id.activity_advanced_feature:
                        Toast.makeText(MainActivity.this, "Navigation Item 04 Clicked", Toast.LENGTH_SHORT).show();
                        intent=new Intent(MainActivity.this, AdvancedFeature.class);
                        startActivity(intent);
                        break;
                }
                return true;
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.drawer_view, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        Intent intent;
        switch (id) {
            case R.id.nav_first_activity:
                Fragment fragment = null;
                fragment = new AboutMe();
                if ( findViewById (R.id.frame_container ) != null && fragment !=
                        null ) {
                    getSupportFragmentManager().beginTransaction ().add (R.id.frame_container ,
                            fragment ). commit () ;
                }
                break;
            case R.id.nav_second_activity:
                Toast.makeText(MainActivity.this, "Navigation Item 02 Clicked", Toast.LENGTH_SHORT).show();
                intent=new Intent(MainActivity.this, Task2.class);
                startActivity(intent);
                break;

            case R.id.nav_third_activity:
                Toast.makeText(MainActivity.this, "Navigation Item 03 Clicked", Toast.LENGTH_SHORT).show();
                intent=new Intent(MainActivity.this, Task3.class);
                startActivity(intent);
                break;
            case R.id.activity_advanced_feature:
                Toast.makeText(MainActivity.this, "Navigation Item 04 Clicked", Toast.LENGTH_SHORT).show();
                intent=new Intent(MainActivity.this, AdvancedFeature.class);
                startActivity(intent);
                break;
            default:
                return super.onOptionsItemSelected(item);
//            case R.id.action_settings:
//                return true;
        }
        mDrawerLayout.openDrawer(GravityCompat.START);
        return true;
    }

}