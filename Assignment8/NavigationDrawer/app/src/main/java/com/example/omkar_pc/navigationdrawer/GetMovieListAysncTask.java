package com.example.omkar_pc.navigationdrawer;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.v4.util.LruCache;
import android.util.Log;
import android.widget.ImageView;

import java.lang.ref.WeakReference;

/**
 * Created by omkar on 3/22/2017.
 */

public class GetMovieListAysncTask extends AsyncTask<String,Void,Bitmap>{

    private final WeakReference<ImageView>imgviewRef;
    LruCache<String,Bitmap>mImgMemory;
    public GetMovieListAysncTask(ImageView img,LruCache<String,Bitmap>mImgMemory) {
        imgviewRef=new WeakReference<ImageView>(img);
       this.mImgMemory=mImgMemory;
    }

    @Override
    protected Bitmap doInBackground(String... urls) {
        Bitmap bitmap=null;
        for(String url:urls){
        bitmap=MyUtility.downloadImageusingHTTPGetRequest(url);
        if(bitmap!=null){
            mImgMemory.put(url,bitmap);
        }
        }
        Log.d("bitmap",String.valueOf(bitmap));
        return bitmap;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
       if(imgviewRef!=null && bitmap!=null){
           final  ImageView imgView=imgviewRef.get();
           if(imgView!=null){

               imgView.setImageBitmap(bitmap);
           }
       }
    }
}
