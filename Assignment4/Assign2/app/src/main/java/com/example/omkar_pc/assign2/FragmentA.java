package com.example.omkar_pc.assign2;

//import android.app.Fragment;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.widget.Button;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
/**
 * Created by OmkarPatil on 2/9/17.
 */

public class FragmentA extends Fragment implements View.OnClickListener{
    Button button,button1,button2;
    public FragmentA(){}

    public View onCreateView ( LayoutInflater inflater , ViewGroup
            container , Bundle savedInstanceState ) {
// Inflate the layout for this fragment
        View rootView =  inflater . inflate ( R.layout.fragment_task0 , container , false );
        button=(Button) rootView.findViewById(R.id.aboutMeButton);
        button1=(Button) rootView.findViewById(R.id.fragTask2);

        button.setOnClickListener(this);
        button1.setOnClickListener(this);

                return rootView;
    }


    public void onClick(View view) {
        Fragment fragment = null;
        switch (view.getId()) {
            case R.id.aboutMeButton:
                fragment = new AboutMe();
                replaceFragment(fragment);
                break;
            case R.id.fragTask2:
                Intent i1 = new Intent(getActivity(), Task2.class);
                startActivity(i1);
                break;
        }
    }
    public void replaceFragment(Fragment someFragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_aboutMe, someFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}