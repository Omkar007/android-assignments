package com.example.omkar_pc.navigationdrawer;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.v4.util.LruCache;
import android.support.v7.widget.CardView;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Omkar-PC on 2/16/2017.
 */

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder>
{
    MovieDataJson utility=new MovieDataJson();


    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
        public void onItemLongClick(View view, int position);
    }

    private List <Map<String, ?>> MovieDataSet;
    private Context mContext;
    OnItemClickListener mItemClickListener;
    LruCache<String,Bitmap> mImgMemorycache;

    RecyclerAdapter(Context myContext, List <Map<String, ?>>  dataSet,LruCache<String,Bitmap>mImgMemorycache){
        this.mContext = myContext;
        MovieDataSet = dataSet;
        this.mImgMemorycache=mImgMemorycache;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        CardView cardView;
        TextView MovieName;
        TextView MovieDescription;
        ImageView MovieImage;
        RatingBar MovieRating;
        CheckBox MovieCheckBox;

        ViewHolder(View itemView) {

            super(itemView);
            cardView = (CardView)itemView.findViewById(R.id.cardview);

            MovieName = (TextView)itemView.findViewById(R.id.movie_title);
            MovieDescription = (TextView)itemView.findViewById(R.id.movie_description);
            MovieImage = (ImageView)itemView.findViewById(R.id.movie_image);
            MovieRating = (RatingBar)itemView.findViewById(R.id.ratingBar);

           final ImageView inflatePopup = (ImageView) itemView.findViewById(R.id.imageView3);
            inflatePopup.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    PopupMenu popup = new PopupMenu(mContext, inflatePopup);
                    popup.getMenuInflater().inflate(R.menu.menu_infalter_popup, popup.getMenu());
                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        public boolean onMenuItemClick(MenuItem item) {
                            switch(item.getItemId())
                            {
                                case R.id.task2_menu_delete:
                                    HashMap hs = (HashMap) MovieDataSet.get(getAdapterPosition());
                                    utility.deletefromDB(getAdapterPosition(),hs);
                                    MovieDataSet.remove(getAdapterPosition());
                                    notifyItemRemoved(getAdapterPosition());
                                    break;
                                case R.id.task2_menu_duplicate:
                                    HashMap hs1 = (HashMap) MovieDataSet.get(getAdapterPosition());
                                    utility.ADDtoDB(getAdapterPosition(),hs1);
                                    MovieDataSet.add(getAdapterPosition(), hs1);
                                    notifyItemInserted(getAdapterPosition());
                                    break;
                            }
                            return true;
                        }
                    });

                    popup.show();//showing popup menu
                }
            });

            MovieRating.setFocusable(false);
            MovieRating.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    return true;
                }
            });

            itemView.setOnClickListener (new View.OnClickListener (){
                @Override
                public void onClick (View v) {
                    if( mItemClickListener != null ){
                        if( getAdapterPosition() != RecyclerView.NO_POSITION ) {
                            mItemClickListener.onItemClick (v, getAdapterPosition());
                        }
                    }
                }
            });
            itemView.setOnLongClickListener (new View.OnLongClickListener () {
                @Override
                public boolean onLongClick(View v) {
                    if (mItemClickListener != null) {
                        if (getAdapterPosition() != RecyclerView.NO_POSITION) {
                            mItemClickListener.onItemLongClick(v, getAdapterPosition());
                        }
                    }
                    return true;
                }
            });
        }
    }

    //@Override
    public void setOnItemClickListener(final OnItemClickListener mItemClickListener)
    {
        this.mItemClickListener = mItemClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.rv_item, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder MovieViewHolder, int i) {

        Map<String, ?> myMap = MovieDataSet.get(i);

  //      MovieViewHolder.MovieImage.setImageResource((Integer)myMap.get("image"));
        MovieViewHolder.MovieName.setText((String) myMap.get("name"));
        MovieViewHolder.MovieDescription.setText((String) myMap.get("description"));
        MovieViewHolder.MovieRating.setRating((((Double) myMap.get("rating")).floatValue())/2);
        GetMovieListAysncTask task=new GetMovieListAysncTask(MovieViewHolder.MovieImage,mImgMemorycache);
        task.execute((String)myMap.get("url"));
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public int getItemCount() {
        return MovieDataSet.size();
    }

    public void changeIdentifier() {
        notifyItemRangeChanged(0, MovieDataSet.size());
        notifyDataSetChanged();
    }

    public void removeAt(int position) {
        MovieDataSet.remove(position);
    }

//    public void DuplicateCardItem(int position) {
//        final JSONObject json;
//
//        HashMap map = (HashMap) MovieDataSet.get(position);
//        if(map!=null){
//            String newname= (String) map.get("name");
//            newname=newname+"_new";
//            map.put("id",newname);
//            json=new JSONObject(map);
//            Log.d("JSON TO ADD DB:",""+json);
//        }
//        else{
//            json=null;
//        }
//            Runnable runnable =new Runnable() {
//                @Override
//                public void run() {
//                    MyUtility.sendHttPostRequest(addURL,json);
//                }
//            };
//            new Thread(runnable).start();
//
//        MovieDataSet.add(position, map);
//       notifyItemInserted(position);
//    }


    public void deleteCardItem() {

        for(int i = 0; i < MovieDataSet.size(); i++) {
            HashMap map = (HashMap) MovieDataSet.get(i);

            if((Boolean)map.get("selection")) {
                MovieDataSet.remove(i);
                notifyItemRemoved(i);
                i--;
            }
        }
        notifyItemRangeChanged(0, MovieDataSet.size());
    }

}
