package com.example.omkar_pc.navigationdrawer;

import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public class Task3 extends AppCompatActivity {

    TextView title;
    MovieData movieData;
    RecyclerAdapter adapter;
    RecyclerView recyclerView;
    static boolean isVisible = true;
    private ActionMode acMode;
    ImageView button;
    SearchView mSearchView;
    Toolbar bottomToolbar;
    ImageView SortToolbar;
    ImageView sortbyYear;
    ImageView sortbyName;
    String sendSub,sendBody;
    int startPos;

    private ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            // TODO Auto-generated method stub
            switch (item.getItemId()) {
                case R.id.deleteItem:
                    adapter.deleteCardItem();
                    mode.finish();
                    return true;

                case R.id.duplicateItem:

                    HashMap map = movieData.getItem(startPos);
                    map.put("selection", false);
                    movieData.getMoviesList().add(startPos + 1, map);
                    adapter.notifyItemInserted(startPos);

                    mode.finish();
                    return true;

                default:
                    return false;
            }
        }

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            // TODO Auto-generated method stub
            mode.getMenuInflater().inflate(R.menu.task2_menu, menu);
            return true;
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            // TODO Auto-generated method stub

        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            // TODO Auto-generated method stub
            return false;
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task3);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        title = (TextView) findViewById(R.id.tvToolBarTitle);
        title.setText("Movie List");


        button = (ImageView) findViewById(R.id.shareButton);
        button.setVisibility(View.INVISIBLE);
        shareInformation();
        recyclerView = (RecyclerView)findViewById(R.id.RecView);
        recyclerView.setHasFixedSize(true);

        bottomToolbar = (Toolbar) findViewById(R.id.bottomToolbar);
        bottomToolbar.setVisibility(View.VISIBLE);
        SortToolbar= (ImageView) findViewById(R.id.sortNameButton);
        sortbyName = (ImageView) findViewById(R.id.sortByName);
        sortbyYear = (ImageView) findViewById(R.id.sortnumber);
        createSortToolbar();
        callSortName();
        callSorYear();
        LinearLayoutManager llm = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(llm);
        movieData = new MovieData();
        adapter = new RecyclerAdapter(this, movieData.getMoviesList());
        recyclerView.setAdapter(adapter);

        adapter.setOnItemClickListener(new RecyclerAdapter.OnItemClickListener() {

            @Override
            public void onItemClick(View v, int position) {
                sendSub = (String) movieData.getItem(position).get("name");
                sendBody = (String) movieData.getItem(position).get("description");
                title.setText(sendSub);

                button.setVisibility(View.VISIBLE);
                title.setText((String) movieData.getItem(position).get("name"));

                fragment_movie MyMovieFrag = fragment_movie .newInstance(position, movieData);
                AppCompatActivity activity = (AppCompatActivity) v.getContext();
                activity.getSupportFragmentManager().beginTransaction()
                        .replace(R.id.frag_container, MyMovieFrag)
                        .addToBackStack(null)
                        .commit();

                getSupportFragmentManager().addOnBackStackChangedListener(
                        new FragmentManager.OnBackStackChangedListener() {
                            public void onBackStackChanged() {
                                int onBackClick = getSupportFragmentManager().getBackStackEntryCount();
                                if(onBackClick == 0){
                                    title.setText("Movie List");
                                    button.setVisibility(View.INVISIBLE);
                                }
                            }
                        });
            }
            @Override
            public void onItemLongClick(View v, int position) {
                acMode = Task3.this.startActionMode(mActionModeCallback);
                startPos = position;
            }
        });
    }

    void shareInformation()
    {
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, sendSub);
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, sendBody);
                startActivity(Intent.createChooser(sharingIntent, "Sharing options"));
            }
        });
    }
    void createSortToolbar() {
        SortToolbar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (true == isVisible) {
                    bottomToolbar.setVisibility(View.INVISIBLE);
                    isVisible = false;
                } else {
                    bottomToolbar.setVisibility(View.VISIBLE);
                    isVisible = true;
                }
                callSortName();
                callSorYear();
            }
        });
    }

    public void callSortName(){
        sortbyName.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                class Sortable implements Comparator<Map<String, ?>> {
                    @Override
                    public int compare(Map<String, ?> map2, Map<String, ?> map1) {
                        String s1 = (String)map1.get("name");
                        String s2 = (String)map2.get("name");
                        int i = s1.compareTo(s2);
                        return i >= 0 ? -1 : 1;
                    }
                }
                Collections.sort(movieData.getMoviesList(), new Sortable());
                adapter.changeIdentifier();
            }
        });
    }
    public void callSorYear(){
        sortbyYear.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                class Sortable implements Comparator<Map<String, ?>> {
                    @Override
                    public int compare(Map<String, ?> map2, Map<String, ?> map1) {
                        Integer y1 = Integer.parseInt((String)map1.get("year"));
                        Integer y2 = Integer.parseInt((String)map2.get("year"));
                        return y1>y2?1:-1;
                    }
                }
                Collections.sort(movieData.getMoviesList(), new Sortable());
                adapter.changeIdentifier();
            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_search, menu);
        MenuItem searchViewItem = menu.findItem(R.id.action_search);
        final SearchView searchViewAndroidActionBar = (SearchView) MenuItemCompat.getActionView(searchViewItem);

        searchViewAndroidActionBar.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchViewAndroidActionBar.clearFocus();
                int index = movieData.getPosition(query);
                recyclerView.getLayoutManager().scrollToPosition(index);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }
}
