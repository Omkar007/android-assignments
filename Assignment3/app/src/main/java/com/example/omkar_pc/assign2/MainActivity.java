package com.example.omkar_pc.assign2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.content.Intent;
//import android.app.Fragment;
//import android.app.FragmentManager;
//import android.app.FragmentTransaction;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.widget.Button;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    private boolean isAboutMe = true ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        Button button1 = ( Button ) findViewById (R. id .aboutMeButton ) ;
        button1 . setOnClickListener ( new Button . OnClickListener () {
            @Override
            public void onClick ( View v ) {

                switchFragment () ;
            }
        }) ;
    }
    public void switchFragment () {
        Fragment fr ;
        if ( isAboutMe ) {
            fr = new AboutMe () ;
        } else {
            fr = new FragmentA () ;
        }
        isAboutMe = ( isAboutMe ) ? false : true ;
        android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft . replace (R . id .frag_frontPage , fr );
        ft . commit () ;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.layout.menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.menu_task2:
                Intent i = new Intent(MainActivity.this, Task1.class);
                startActivity(i);
                return true;
            case R.id.menu_task3:
                Intent i1 = new Intent(MainActivity.this, Task2.class);
                startActivity(i1);
                return true;
            case R.id.menu_task4:
                Intent i2 = new Intent(MainActivity.this, Task3.class);
                startActivity(i2);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
