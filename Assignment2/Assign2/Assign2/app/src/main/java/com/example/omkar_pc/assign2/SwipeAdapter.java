package com.example.omkar_pc.assign2;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;

/**
 * Created by omkar patil on 2/9/17.
 */

public class SwipeAdapter extends FragmentStatePagerAdapter {
    final int PAGE_COUNT = 30;

    public SwipeAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int i) {
        Fragment fragment= new moviefragment();
        Bundle bundle=new Bundle();
        MovieData movieData = new MovieData();

        bundle.putInt("count",(i+1));

        Map<String, ?> myMap = movieData.getItem(i);

        bundle.putInt("imageChange",(Integer)myMap.get("image"));
        bundle.putString("name",(String) myMap.get("name"));
        bundle.putString("description",(String) myMap.get("description"));
        bundle.putString("year",(String) myMap.get("year"));
        bundle.putString("length",(String) myMap.get("length"));
        bundle.putString("rating",Double.toString ((Double) myMap.get("rating")));
        bundle.putString("director",(String) myMap.get("director"));
        bundle.putString("stars",(String) myMap.get("stars"));
        bundle.putString("url",(String) myMap.get("url"));

        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;

    }
    public CharSequence getPageTitle(int i) {
        // Generate title based on item position
        MovieData movieData = new MovieData();
        Map<String, ?> myMap = movieData.getItem(i);
        return (String)myMap.get("name");
    }
}
