package com.example.omkar_pc.navigationdrawer;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Omkar-PC on 2/16/2017.
 */

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder>
{
    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
        public void onItemLongClick(View view, int position);
    }

    private List <Map<String, ?>> MovieDataSet;
    private Context mContext;
    OnItemClickListener mItemClickListener;

    RecyclerAdapter(Context myContext, List <Map<String, ?>>  dataSet){
        this.mContext = myContext;
        MovieDataSet = dataSet;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        CardView cardView;
        TextView MovieName;
        TextView MovieDescription;
        ImageView MovieImage;
        RatingBar MovieRating;
        CheckBox MovieCheckBox;

        ViewHolder(View itemView) {

            super(itemView);
            cardView = (CardView)itemView.findViewById(R.id.cardview);

            MovieName = (TextView)itemView.findViewById(R.id.movie_title);
            MovieDescription = (TextView)itemView.findViewById(R.id.movie_description);
            MovieImage = (ImageView)itemView.findViewById(R.id.movie_image);
            MovieRating = (RatingBar)itemView.findViewById(R.id.ratingBar);
          //  MovieCheckBox = (CheckBox)itemView.findViewById(R.id.checkBox);

//            MovieCheckBox.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if(MovieCheckBox.isChecked()) {
//                        HashMap map = (HashMap) MovieDataSet.get(getAdapterPosition());
//                        map.put("selection", true);
//                        MovieDataSet.set(getAdapterPosition(), map);
//                   //     System.out.println("checked: " +(Boolean)map.get("selection"));
//                    }
//                    else
//                    {
//                        HashMap map = (HashMap) MovieDataSet.get(getAdapterPosition());
//                        map.put("selection", false);
//                        MovieDataSet.set(getAdapterPosition(), map);
//                  //      System.out.println("UnChecked: " + (Boolean)map.get("selection"));
//                    }
//                }
//            });

           final ImageView inflatePopup = (ImageView) itemView.findViewById(R.id.imageView3);
            inflatePopup.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    PopupMenu popup = new PopupMenu(mContext, inflatePopup);
                    popup.getMenuInflater().inflate(R.menu.menu_infalter_popup, popup.getMenu());
                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        public boolean onMenuItemClick(MenuItem item) {
                            switch(item.getItemId())
                            {
                                case R.id.task2_menu_delete:
                                    removeAt(getAdapterPosition());
                                    break;
                                case R.id.task2_menu_duplicate:
                                    DuplicateCardItem(getAdapterPosition());
                                    break;
                            }
                            return true;
                        }
                    });

                    popup.show();//showing popup menu
                }
            });

            MovieRating.setFocusable(false);
            MovieRating.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    return true;
                }
            });

            itemView.setOnClickListener (new View.OnClickListener (){
                @Override
                public void onClick (View v) {
                    if( mItemClickListener != null ){
                        if( getAdapterPosition() != RecyclerView.NO_POSITION ) {
                            mItemClickListener.onItemClick (v, getAdapterPosition());
                        }
                    }
                }
            });
            itemView.setOnLongClickListener (new View.OnLongClickListener () {
                @Override
                public boolean onLongClick(View v) {
                    if (mItemClickListener != null) {
                        if (getAdapterPosition() != RecyclerView.NO_POSITION) {
                            mItemClickListener.onItemLongClick(v, getAdapterPosition());
                        }
                    }
                    return true;
                }
            });
        }
    }

    //@Override
    public void setOnItemClickListener(final OnItemClickListener mItemClickListener)
    {
        this.mItemClickListener = mItemClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.rv_item, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder MovieViewHolder, int i) {

        Map<String, ?> myMap = MovieDataSet.get(i);

        MovieViewHolder.MovieImage.setImageResource((Integer)myMap.get("image"));
        MovieViewHolder.MovieName.setText((String) myMap.get("name"));
        MovieViewHolder.MovieDescription.setText((String) myMap.get("description"));
        MovieViewHolder.MovieRating.setRating((((Double) myMap.get("rating")).floatValue())/2);
//        MovieViewHolder.MovieCheckBox.setChecked((Boolean)myMap.get("selection"));
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public int getItemCount() {
        return MovieDataSet.size();
    }

    public void changeIdentifier() {
        notifyItemRangeChanged(0, MovieDataSet.size());
        notifyDataSetChanged();
    }
    //@Override
    public void removeAt(int position) {
        MovieDataSet.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, MovieDataSet.size());
    }

    public void DuplicateCardItem(int position) {
        HashMap map = (HashMap) MovieDataSet.get(position);
        map.put("selection", false);
        MovieDataSet.add(position + 1, map);
        notifyItemInserted(position);
    }

    public void deleteCardItem() {

        for(int i = 0; i < MovieDataSet.size(); i++) {
            HashMap map = (HashMap) MovieDataSet.get(i);

            if((Boolean)map.get("selection")) {
                MovieDataSet.remove(i);
                notifyItemRemoved(i);
                i--;
            }
        }
        notifyItemRangeChanged(0, MovieDataSet.size());
    }

}
