package com.example.omkar_pc.assign2;

import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.support.v7.widget.Toolbar;
import android.support.design.widget.TabLayout;

public class Task2 extends AppCompatActivity {
    ViewPager viewpager;
    FragmentStatePagerAdapter swipeAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task2);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        viewpager=(ViewPager)findViewById(R.id.container);
        swipeAdapter=new SwipeAdapter(getSupportFragmentManager());
        viewpager.setAdapter(swipeAdapter);
        viewpager.setCurrentItem(0);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.sliding_tabs);
        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        tabLayout.setupWithViewPager(viewpager);
    }

}
