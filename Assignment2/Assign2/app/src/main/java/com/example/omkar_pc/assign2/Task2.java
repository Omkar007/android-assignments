package com.example.omkar_pc.assign2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class Task2 extends AppCompatActivity {

    Button b0,b1,b2,b3,b4,b5,b6,b7,b8,b9,bAdd,bMul,bSub,bDiv,bPercent,bEquals;
    TextView editText3;
    int val1,val2;
    boolean addition,subtraction,multiplication,division,percent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task2);
        b1=(Button) findViewById(R.id.button7);
        b2=(Button) findViewById(R.id.button9);
        b3=(Button) findViewById(R.id.button2);
        b4=(Button) findViewById(R.id.button4);
        b5=(Button) findViewById(R.id.button12);
        b6=(Button) findViewById(R.id.button11);
        b7=(Button) findViewById(R.id.button6);
        b8=(Button) findViewById(R.id.button14);
        b9=(Button) findViewById(R.id.button10);
        b0=(Button) findViewById(R.id.button18);
        bAdd=(Button) findViewById(R.id.button15);
        bMul=(Button) findViewById(R.id.buttonMul);
        bSub=(Button) findViewById(R.id.button21);
        bDiv=(Button) findViewById(R.id.button20);
        bPercent=(Button) findViewById(R.id.button16);
        bEquals=(Button) findViewById(R.id.button19);
        editText3=(TextView) findViewById(R.id.editText3);

        b1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                editText3.setText(editText3.getText()+"1");
            }
        });

        b2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                editText3.setText(editText3.getText()+"2");
            }
        });

        b3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                editText3.setText(editText3.getText()+"3");
            }
        });
        b4.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                editText3.setText(editText3.getText()+"4");
            }
        });
        b5.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                editText3.setText(editText3.getText()+"5");
            }
        });
        b6.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                editText3.setText(editText3.getText()+"6");
            }
        });
        b7.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                editText3.setText(editText3.getText()+"7");
            }
        });
        b8.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                editText3.setText(editText3.getText()+"8");
            }
        });
        b9.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                editText3.setText(editText3.getText()+"9");
            }
        });
        b0.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                editText3.setText(editText3.getText()+"0");
            }
        });

        bAdd.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                val1=Integer.parseInt(editText3.getText()+"");
                addition=true;
                editText3.setText(null);
            }
        });
        bSub.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                val1=Integer.parseInt(editText3.getText()+"");
                subtraction=true;
                editText3.setText(null);
            }
        });
        bMul.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                val1=Integer.parseInt(editText3.getText()+"");
                multiplication=true;
                editText3.setText(null);
            }
        });
        bDiv.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                val1=Integer.parseInt(editText3.getText()+"");
                division=true;
                editText3.setText(null);
            }
        });
        bEquals.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                val2=Integer.parseInt(editText3.getText()+"");
                    if(addition){
                        editText3.setText(val1+val2+"");
                        addition=false;
                    }
                    if(subtraction){
                        editText3.setText(val1-val2+"");
                        subtraction=false;
                    }
                    if(multiplication){
                        editText3.setText(val1*val2+"");
                        multiplication=false;
                    }
                    if(division){
                        editText3.setText(val1/val2+"");
                        division=false;
                    }
                    if(percent){
                        int valcalc=val1/100;
                        editText3.setText(valcalc*val2+"");
                        percent=false;
                    }
            }
        });

    }

}
