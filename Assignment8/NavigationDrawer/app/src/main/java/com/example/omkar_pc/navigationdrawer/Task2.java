package com.example.omkar_pc.navigationdrawer;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.util.LruCache;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.HashMap;

public class Task2 extends AppCompatActivity
{
    TextView title;
    MovieData movieData;
    RecyclerAdapter adapter;
    RecyclerView recyclerView;
    private ActionMode acMode;
    ImageButton button;
    SearchView mSearchView;
    String sendSub,sendBody;
    int startPos;
    private String url="http://omkar.com/index.php/movies";
    private String ratingUrl="http://omkar.com/index.php/movies/rating/";
    private String idUrl="http://omkar.com/index.php/movies/id/";
    LruCache<String,Bitmap>mImgMemorycache;

    private ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            // TODO Auto-generated method stub
            switch (item.getItemId()) {
                case R.id.deleteItem:
                    adapter.removeAt(startPos);
                    adapter.notifyDataSetChanged();
                    mode.finish();
                    return true;

                case R.id.duplicateItem:

                    HashMap map = movieData.getItem(startPos);
                    map.put("selection", false);
                    movieData.getMoviesList().add(startPos + 1, map);
                    adapter.notifyItemInserted(startPos);

                    mode.finish();
                    return true;

                default:
                    return false;
            }
        }

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            // TODO Auto-generated method stub
            mode.getMenuInflater().inflate(R.menu.task2_menu, menu);
            return true;
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            // TODO Auto-generated method stub

        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            // TODO Auto-generated method stub
            return false;
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task2);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        title = (TextView) findViewById(R.id.tvToolBarTitle);
        title.setText("Movie List");


        button = (ImageButton) findViewById(R.id.shareButton);
        button.setVisibility(View.INVISIBLE);
        shareInformation();
        recyclerView = (RecyclerView)findViewById(R.id.RecView);
        recyclerView.setHasFixedSize(true);

        LinearLayoutManager llm = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(llm);
        movieData = new MovieData();
        //creating cache
        if(mImgMemorycache==null){
            final int maxMemory=(int)(Runtime.getRuntime().maxMemory()/1024);
            final int cacheSize=maxMemory/8;

            mImgMemorycache=new LruCache<String,Bitmap>(cacheSize){
                @Override
                protected int sizeOf(String key, Bitmap value) {
                    return value.getByteCount()/1024;
                }
            };
        }
        adapter = new RecyclerAdapter(this, movieData.getMoviesList(),mImgMemorycache);
        recyclerView.setAdapter(adapter);
        MoviedataDownloadAsync movieAsyncTask=new MoviedataDownloadAsync(adapter,movieData);
        movieAsyncTask.execute(url);
        adapter.setOnItemClickListener(new RecyclerAdapter.OnItemClickListener() {

            @Override
            public void onItemClick(View v, int position) {
                sendSub = (String) movieData.getItem(position).get("name");
                sendBody = (String) movieData.getItem(position).get("description");
                title.setText(sendSub);
                button.setVisibility(View.VISIBLE);
                title.setText((String) movieData.getItem(position).get("name"));

                fragment_movie MyMovieFrag = fragment_movie .newInstance(position, movieData,adapter,mImgMemorycache);
                AppCompatActivity activity = (AppCompatActivity) v.getContext();
                activity.getSupportFragmentManager().beginTransaction()
                        .replace(R.id.frag_container, MyMovieFrag)
                        .addToBackStack(null)
                        .commit();

                getSupportFragmentManager().addOnBackStackChangedListener(
                        new FragmentManager.OnBackStackChangedListener() {
                            public void onBackStackChanged() {
                                int onBackClick = getSupportFragmentManager().getBackStackEntryCount();
                                if(onBackClick == 0){
                                    title.setText("Movie List");
                                    button.setVisibility(View.INVISIBLE);
                                }
                            }
                        });
            }
            @Override
            public void onItemLongClick(View v, int position) {
                acMode = Task2.this.startActionMode(mActionModeCallback);
                startPos = position;
            }
        });
    }

    void shareInformation()
    {
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, sendSub);
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, sendBody);
                startActivity(Intent.createChooser(sharingIntent, "Sharing options"));
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_search, menu);
        MenuItem searchViewItem = menu.findItem(R.id.action_search);
        final SearchView searchViewAndroidActionBar = (SearchView) MenuItemCompat.getActionView(searchViewItem);

        searchViewAndroidActionBar.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchViewAndroidActionBar.clearFocus();
                String queryUrl=ratingUrl+query;
                MoviedataDownloadAsync movieAsyncTask=new MoviedataDownloadAsync(adapter,movieData);
                movieAsyncTask.execute(queryUrl);
              //  int index = movieData.getPosition(query);
              //  recyclerView.getLayoutManager().scrollToPosition(index);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }
}
