<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require "../myslim/vendor/autoload.php";
require "../omkar/movie.php";

$app = new \Slim\App;

//Get Movies List
 $app->get('/movieList',
         function(){
         getMovieList();
  });
  
  
 //Get MovieDetails
  $app->get('/movieList/id/{movieId}',
          function($request,$response,$args){
          getMovieDetails($args['movieId']);
          }
  );

  //retrieve movies more than argument
  $app->get('/movieList/rating/{movieRating}',
        function($request,$response,$args){
        getMoviesWithRating($args['movieRating']);
        }
  );
  
 
  //delete operation  for form
 $app->get(
      '/deleteMovie', 
	function() {
		echo "<h1 style='color:red;'> Remove Movie Details</h1>";
		echo "<form id = \"formName\" action=\"http://www.omkar.com/index.php/deleteMovie\" method=\"post\">";
		echo "<pre>Enter movie Id: \t<input type=\"text\"name=\"id\"><br/></pre><br/>";
		echo "<input type=\"submit\" value=\"Submit\"><br\>";
		echo "</form>"; 
});

//delete method implemented for post
$app->post(
 	'/deleteMovie',
 	function($request, $response){
		//$movieName = json_decode($request->getBody(),true);
		$movieName = $request->getParsedBody();
		deleteMovie($movieName['id']);
 });
  
  //Funtion to delete movie
 function deleteMovie($movieId){
 $conn =getDB();
 $sql = "DELETE FROM movies WHERE id='".$movieId."'";
 $conn->query($sql);
 $conn->query("commit");
 echo "<div style ='font:18px/21px Arial,tahoma,sans-serif;color:#ff0000'>Movie deleted successfully</div>";
 $conn->close();
 }
 
  //Generate form for Adding movie
  $app->get('/', 
	function() {
		echo "<h2 style='color:red;'>Android Programming Assignment 7</h2>";
		echo "<form action=\"http://www.omkar.com/index.php/add\" method=\"post\">";
		echo "Movie Id: \t<input type=\"text\" name=\"id\"><br><br>";
		echo "Movie Name: <input type=\"text\" name=\"name\"><br><br>";
		echo "Description: <input type=\"text\" name=\"description\"><br><br>";
		echo "Movie Stars: <input type=\"text\" name=\"stars\"><br><br>";
		echo "Movie Length: <input type=\"text\" name=\"length\"><br><br>";
		echo "Image: <input type=\"text\" name=\"image\"><br><br>";
		echo "Year: <input type=\"text\" name=\"year\"><br><br>";
		echo "Director: <input type=\"text\" name=\"director\"><br><br>";
		echo "Movie URL: <input type=\"text\" name=\"url\"><br><br>";
		echo "Add Movie Details: <input type=\"submit\" value=\"Submit\">";
		echo "</form>"; 
});

 
//add method to post data to db using rest 
 $app->post('/add', 
function(Request $request,Response $response) {
	$data = json_decode($request->getBody(),true);


	$conn = getDb();
	if ($stmt = $conn->prepare("INSERT INTO Movies (id, name, description, stars, length, image, year, director, url)
					VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)" )){
		$stmt->bind_param("sssssssss", $data['id'], $data['name'], $data['description'], $data['stars'],
						$data['length'], $data['image'], $data['year'], $data['director'], $data['url']);
		
		$stmt->execute();
		echo "<div style ='font:18px/21px Arial,tahoma,sans-serif;color:#ff0000'>Added movie details to AndroidMovieDB successfully\n </div>";
		$stmt->close();
	}
	else{
		echo $stmt."<p>";
		die('Error in adding data to database [' . $conn->error . ']');
	}
	$conn->close();
});

 
 $app->run();

?>