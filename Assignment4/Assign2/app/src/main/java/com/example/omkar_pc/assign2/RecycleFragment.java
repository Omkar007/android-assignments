package com.example.omkar_pc.assign2;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.support.v7.widget.RecyclerView;
import android.view.animation.OvershootInterpolator;
import android.widget.Button;
import android.widget.Toast;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jp.wasabeef.recyclerview.animators.SlideInLeftAnimator;
import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;

/**
 * Created by Omkar-PC on 16-02-2017.
 */

public class RecycleFragment extends Fragment{
    private MovieData movieItems;
    Button selectButton,clearButton,deleteButton,sortButton;
    private RecyclerView recyclerView;
    private LinearLayoutManager layoutManager;
            MyRVAdapter myAdapter=null;

    public RecycleFragment(){movieItems = new MovieData();}

    public View onCreateView ( LayoutInflater inflater , ViewGroup container,Bundle savedInstanceState ) {

        View currentView = inflater . inflate (R.layout . fragment_rv , container , false );
        recyclerView = ( RecyclerView ) currentView . findViewById (R.id .recycler_view2 ) ;
        recyclerView . setHasFixedSize ( true );
        layoutManager = new LinearLayoutManager ( currentView . getContext () );
        recyclerView . setLayoutManager ( layoutManager ) ;
        myAdapter = new MyRVAdapter (movieItems) ;
        recyclerView . setAdapter ( myAdapter );
        recyclerView . setItemAnimator( new SlideInUpAnimator(new OvershootInterpolator(1f)));
        myAdapter . setOnItemClickListener (new MyRVAdapter.OnItemClickListener () {
                                                        @Override
                                                        public void onItemClick ( View view , int position ) {
                                                            Toast.makeText ( getContext () , " Move "+ ( position +1) +" Selected " , Toast . LENGTH_SHORT ). show () ;
                                                            android.support.v4.app.FragmentTransaction ft = ((AppCompatActivity)view.getContext()).getSupportFragmentManager().beginTransaction();
                                                            ft . replace (R . id .fragment_movie , GetMovieFragment(position) );
                                                            ft . addToBackStack(null);
                                                            ft . commit () ;

                                                        }
                                                        @Override
                                                        public void onItemLongClick ( View view , int position ) {

                                                                  HashMap map=movieItems.getItem(position);
                                                                    map.put("selection",false);
                                                                  movieItems.getMoviesList().add(position+1,map);
                                                                    myAdapter.notifyItemInserted(position);

                                                        }
                                                    });

        selectButton=(Button) currentView.findViewById(R.id.selectAll);
        clearButton=(Button) currentView.findViewById(R.id.ClearAll);
        deleteButton=(Button) currentView.findViewById(R.id.delete);
        sortButton=(Button) currentView.findViewById(R.id.sort);

        selectButton.setOnClickListener( new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
               // Toast.makeText ( getContext () ," Select ALL " , Toast . LENGTH_SHORT ). show () ;
                HashMap map=null;
                for(int i=0;i<movieItems.getMoviesList().size();i++){
                    map=movieItems.getItem(i);
                    map.put("selection",true);
                    movieItems.getMoviesList().set(i,map);
                    myAdapter.notifyItemRangeChanged(0, movieItems.getMoviesList().size());

                }

            }
        });

        clearButton.setOnClickListener( new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                //Toast.makeText ( getContext () ," clearButton ALL " , Toast . LENGTH_SHORT ). show () ;
                HashMap map=null;
                for(int i=0;i<movieItems.getMoviesList().size();i++) {
                    map = movieItems.getItem(i);
                    map.put("selection", false);
                    movieItems.getMoviesList().set(i, map);
                    myAdapter.notifyItemRangeChanged(0, movieItems.getMoviesList().size());

                }
            }
        });

        deleteButton.setOnClickListener( new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                for(int i=0;i<movieItems.getMoviesList().size();i++) {
                    if(movieItems.getMoviesList().get(i).get("selection")==Boolean.TRUE){
                        movieItems.getMoviesList().remove(i);
                        myAdapter.notifyItemRemoved(i);
                        i--;
                    }
                }
                myAdapter.notifyItemRangeChanged(0, movieItems.getMoviesList().size());
            }
        });
        sortButton.setOnClickListener( new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                class SortAdapter implements Comparator<Map<String, ?>> {
                    @Override
                    public int compare(Map<String, ?> map2, Map<String, ?> map1) {
                        Integer year1 = Integer.parseInt((String)map1.get("year"));
                        Integer year2 = Integer.parseInt((String)map2.get("year"));
                        return year1>year2?1:-1;
                    }
                }
                Collections.sort(movieItems.getMoviesList(), new SortAdapter());
                myAdapter.notifyItemRangeChanged(0, movieItems.getMoviesList().size());
            }
        });
        return currentView ;
    }
    Fragment GetMovieFragment(int position)
    {

        Fragment fragment= new MovieFragment();
        Bundle bundle=new Bundle();
        MovieData movieData = new MovieData();

     //   bundle.putInt("count",(position));

        Map<String, ?> myMap = movieData.getItem(position);

        bundle.putInt("imageChange",(Integer)myMap.get("image"));
        bundle.putString("name",(String) myMap.get("name"));
        bundle.putString("description",(String) myMap.get("description"));
        bundle.putString("year",(String) myMap.get("year"));
        bundle.putString("length",(String) myMap.get("length"));
        bundle.putString("rating",Double.toString ((Double) myMap.get("rating")));
        bundle.putString("director",(String) myMap.get("director"));
        bundle.putString("stars",(String) myMap.get("stars"));
        bundle.putString("url",(String) myMap.get("url"));
        fragment.setArguments(bundle);
        return fragment;
    }
}
