package com.example.omkar_pc.assign2;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

/**
 * Created by Omkar-PC on 17-02-2017.
 */

public class MovieFragment extends Fragment {
    TextView movieName, movieDesc, movieYear, movieLength, movieDirector, movieStars, movieUrl;
    ImageView imageView;
    RatingBar ratingBar;
    public MovieFragment(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_movie,container,false);
        movieName = (TextView) view.findViewById(R.id.name_val);
        movieDesc = (TextView) view.findViewById(R.id.desc_val);
        movieYear = (TextView) view.findViewById(R.id.year_val);
        movieLength = (TextView) view.findViewById(R.id.len_val);
        movieDirector = (TextView) view.findViewById(R.id.dir_val);
        movieStars = (TextView) view.findViewById(R.id.star_val);
        movieUrl = (TextView) view.findViewById(R.id.url_val);
        ratingBar=(RatingBar) view.findViewById(R.id.rat_val);
        ViewGroup.LayoutParams params=ratingBar.getLayoutParams();
        params.width = 600;
        ratingBar.setLayoutParams(params);
        ratingBar.setNumStars(5);
        ratingBar.setStepSize(1);
        imageView=(ImageView) view.findViewById(R.id.imageView);
        imageView.getLayoutParams().height = 600;
        Bundle bundle=getArguments();

        movieName.setText(bundle.getString("name"));
        movieDesc.setText(bundle.getString("description"));
        movieYear.setText(bundle.getString("year"));
        movieLength.setText(bundle.getString("length"));
        movieDirector.setText(bundle.getString("director"));
        movieStars.setText(bundle.getString("stars"));
        movieUrl.setText(bundle.getString("url"));
        float rating = Float.parseFloat(bundle.getString("rating"));
        ratingBar.setRating(rating/2);
        imageView.setImageResource(bundle.getInt("imageChange"));
        imageView.requestLayout();

        return view;
    }

}
